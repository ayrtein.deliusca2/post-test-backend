const employees = [
    ['amzar', 1995],
    ['diana', 2010],
    ['ihsan', 2015],
    ['fajri', 2006],
    ['sugeng', 1993],
    ['handoko', 2000],
    ['jundi', 2019],
    ['gede', 2018],
    ['deli', 1015]
]


function sortYear(a, b) {
    if (a[1] === b[1]) {
        return 0;
    }
    else {
        return (a[1] < b[1]) ? -1 : 1;
    }
}

//view 1
console.log("Sorting employee based on start working year : ")
console.log(employees.sort(sortYear));




