const employees = [
    ['amzar', 1995],
    ['diana', 2010],
    ['ihsan', 2015],
    ['fajri', 2006],
    ['sugeng', 1993],
    ['handoko', 2000],
    ['jundi', 2019],
    ['gede', 2018],
    ['deli', 1015]
]
console.log("Filtering employee > 10 years join = ")
employees.forEach(employee => {
    let temp = []
    let currentTime = new Date()
    let year = currentTime.getFullYear()
    let duration = (year - employee[1])
    employee[2] = duration
    if (employee[2] > 10) {
        temp.push(employee)
    }
    temp = temp.filter(item => item);
    console.log(temp)
});


console.log("Filtering employee < 10 years join = ")
employees.forEach(employee => {
    let temp = []
    let currentTime = new Date()
    let year = currentTime.getFullYear()
    let duration = (year - employee[1])
    employee[2] = duration
    if (employee[2] < 10) {
        temp.push(employee)
    }
    temp = temp.filter(item => item);
    console.log(temp)
});