class Human {
    constructor(name, birthDate, address) {
      if (this.constructor.name === 'Human') {
        throw new Error('Cannot instantiate from abstract class')
      }
      this.name = name;
      this.birthDate = birthDate;
      this.address = address;
    }
  
    introduce() {
      console.log(`Hi, my name is ${this.name}`)
    }
  }
  
  // Polymorphism dari kelas Human.
  class Programmer extends Human {
    constructor(name, birthDate, address, programmerLanguages){
        super(name, birthDate, address)
        this.programmerLanguages = programmerLanguages;
    }


    introduce(){
        super.introduce()
        console.log(`I'm a ${this.constructor.name} and master of ${this.programmerLanguages}`)
    }
  }
  class Doctor extends Human {
    constructor(name, birthDate, address, specialist){
        super(name, birthDate, address)
        this.specialist = specialist;
    }

    introduce(){
        console.log(`Hi my name is ${this.name} and I'm a ${this.constructor.name}`)
    }
  }
  class Chef extends Human {
    constructor(name, birthDate, address, cuisine){
        super(name, birthDate, address)
        this.cuisine = cuisine;
    }

    introduce(){
        console.log(`Hi my name is ${this.name} and I'm a ${this.constructor.name}`)
    }

  }
  

  const deli = new Programmer('Deli', '30-07-1999', 'Solo', ['Java', ' C++'])
  console.log(deli.introduce())
  const doctorView = new Doctor('Deli', '30-07-1999', 'Solo', 'dentist')
  console.log(doctorView.introduce())
  const chefView = new Chef('Deli', '30-07-1999', 'Solo', 'Asian')
  console.log(chefView.introduce())