const fruit = ['pisang', 'anggur', 'nangka', 'anggur', 'pisang', 'jeruk', 'jeruk', 'anggur', 'pisang', 'nangka', 'jeruk',
    'jeruk', 'jeruk', 'pisang', 'anggur', 'nangka', 'anggur', 'pisang', 'jeruk nipis', 'jeruk', 'anggur', 'pisang', 'timun', 'timun', 'jeruk', 'jeruk',
    'jeruk', 'jeruk', 'jeruk', 'pisang', 'anggur', 'nangka', 'anggur', 'timun', 'jeruk', 'jeruk', 'timun', 'pisang', 'timun', 'timun', 'jeruk',
    'jeruk nipis', 'jeruk', 'pisang', 'pisang']

function findMostFrequent(fruit) {
    let mf = 1;
    let m = 0;
    let item;

    for (let i = 0; i < fruit.length; i++) {
        for (let j = i; j < fruit.length; j++) {
            if (fruit[i] == fruit[j]) {
                m++;
                if (m > mf) {
                    mf = m;
                    item = fruit[i];
                }
            }
        }
        m = 0;
    }

    return item;
}

function findLessFrequent(fruit) {
    var mif = 99999999999999;
    var m = 0;
    var itemin;
    for (var i = 0; i < fruit.length; i++) {
        for (var j = 0; j < fruit.length; j++) {
            if (fruit[i] == fruit[j])
                m++;
        }
        if (mif > m) {
            mif = m;
            itemin = fruit[i];
        }
    }
    return itemin
}


console.log("The most frequent fruit = " + findMostFrequent(fruit));
console.log("The less frequent fruit = " + findLessFrequent(fruit));