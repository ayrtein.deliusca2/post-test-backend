//console.log("Work in progress")

class Animal {
    constructor(name, amount, price, weight, foot) {
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.weight = weight;
        this.foot = foot;
    }
}

class Fish extends Animal {
    constructor(name, amount, price, weight, foot) {
        super(name, amount, price, weight, foot)
    }

    countFoot(){
        const footFish = this.foot
        return footFish

    }
    getWeight(){
        return this.weight
    }
    getPrice(){
        return this.price
    }
    getAmount(){
        return this.amount
    }
}

class Goat extends Animal {
    constructor(name, amount, price, weight, foot) {
        super(name, amount, price, weight, foot)
    }

    countFoot(){
        const footGoat = this.foot
        return footGoat
    }
    getWeight(){
        return this.weight
    }
    getPrice(){
        return this.price
    }
    getAmount(){
        return this.amount
    }
}

class Chicken extends Animal {
    constructor(name, amount, price, weight, foot) {
        super(name, amount, price, weight, foot)
    }

    countFoot(){
        const footChicken = this.foot
        return footChicken
    }
    getWeight(){
        return this.weight
    }
    getPrice(){
        return this.price
    }
    getAmount(){
        return this.amount
    }
}

class Cow extends Animal {
    constructor(name, amount, price, weight, foot) {
        super(name, amount, price, weight, foot)
    }

    countFoot(){
        const footCow = this.foot
        return footCow
    }
    getWeight(){
        return this.weight
    }
    getPrice(){
        return this.price
    }
    getAmount(){
        return this.amount
    }
}

module.exports = Animal, Fish, Goat, Cow, Chicken

const fish = new Fish('Fish', 120, 25000, 0.5, 0)
const goat = new Goat('Goat', 40, 100000, 20, 4)
const chicken = new Chicken('Chicken', 100, 50000, 1, 2)
const cow = new Cow('Cow', 25, 150000, 50, 4)
//foot
const footFish = fish.countFoot()
const footGoat = goat.countFoot()
const footChicken = chicken.countFoot()
const footCow = cow.countFoot()
//weight
const weightGoat = goat.getWeight()
const weightFish = fish.getWeight()
const weightChicken = chicken.getWeight()
const weightCow = cow.getWeight()
//amount
const amountGoat = goat.getAmount()
const amountFish = fish.getAmount()
const amountChicken = chicken.getAmount()
const amountCow = cow.getAmount()
//price
const priceGoat = goat.getPrice()
const priceFish = fish.getPrice()
const priceChicken = chicken.getPrice()
const priceCow = cow.getPrice()

//Calculate foot
function calculateFoot(){
    return ((footGoat*amountGoat) + (footChicken*amountChicken) + (footCow*amountCow) + (footFish*amountFish))
}

//Calculate weight
function calculateWeightGoat(){
    return (weightGoat*amountGoat)
}
function calculateWeightFish(){
    return (weightFish*amountFish)
}
function calculateWeightChicken(){
    return (weightChicken*amountChicken)
}
function calculateWeightCow(){
    return (weightCow*amountCow)
}
function totalAllWeight(){
    return calculateWeightGoat() + calculateWeightFish() + calculateWeightCow() + calculateWeightChicken()
}

//Calculate price
function calculatePriceGoat(){
    return (amountGoat*weightGoat*priceGoat)
}
function calculatePriceFish(){
    return (amountFish*weightFish*priceFish)
}
function calculatePriceChicken(){
    return (amountChicken*weightChicken*priceChicken)
}
function calculatePriceCow(){
    return (amountCow*weightCow*priceCow)
}
function totalAllPrice(){
    return calculatePriceGoat() + calculatePriceFish() + calculatePriceChicken() + calculatePriceCow()
}

//average price
function calculateAvgPriceGoat(){
    return (weightGoat*priceGoat)
}
function calculateAvgPriceChicken(){
    return (weightChicken*priceChicken)
}
function calculateAvgPriceCow(){
    return (weightCow*priceCow)
}
function calculateAvgPriceFish(){
    return (weightFish*priceFish)
}


console.log("======== STARTING LINE =========")
console.log("1. Count foot of all animal :")
console.log(calculateFoot() + " foot")
console.log("================================")
console.log("2. Count total weight of each animal :")
console.log("Weight of all goat = " + calculateWeightGoat() + " KG")
console.log("Weight of all chicken = " + calculateWeightChicken() + " KG")
console.log("Weight of all cow = " + calculateWeightCow() + " KG")
console.log("Weight of all fish = " + calculateWeightFish() + " KG")
console.log(`Total all animal weight = ${totalAllWeight()} KG`)
console.log("===============================")
console.log("3. Count price weight of all animal :")
console.log("Price of all goat = Rp " + calculatePriceGoat())
console.log("Price of all chicken = Rp " + calculatePriceChicken())
console.log("Price of all cow = Rp " + calculatePriceCow())
console.log("Price of all fish = Rp " + calculatePriceFish())
console.log(`Total all animal price = Rp ${totalAllPrice()}`)
console.log("===============================")
console.log("4. Average price per animal :")
console.log("Average price of goat = Rp " + calculateAvgPriceGoat())
console.log("Average price of chicken = Rp " + calculateAvgPriceChicken())
console.log("Average price of cow = Rp " + calculateAvgPriceCow())
console.log("Average price of fish = Rp " + calculateAvgPriceFish())
console.log("=========== END OF LINE ===========")